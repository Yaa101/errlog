package errlog

import (
	"log"
	"runtime"
)

// DEBUG - set this to true, for debug logging taking place
var DEBUG = true

// Debug - handles error logging while programming, instead of fmt.Printf
func Debug(err error) {

	if DEBUG {
		// notice that we're using 1, so it will actually log the where
		// the error happened, 0 = this function, we don't want that.
		pc, filename, line, _ := runtime.Caller(1)

		log.Printf("[debug] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), filename, line, err)
	}
	return
}

// HandleError - used for error logging
func HandleError(err error) (b bool) {
	if err != nil {
		// notice that we're using 1, so it will actually log where
		// the error happened, 0 = this function, we don't want that.
		_, filename, line, _ := runtime.Caller(1)

		log.Printf("[error] %s:%d %v", filename, line, err)
		b = true
	}
	return
}

// FancyHandleError - similar to HandleError, this logs the function name as well.
func FancyHandleError(err error) (b bool) {
	if err != nil {
		// notice that we're using 1, so it will actually log the where
		// the error happened, 0 = this function, we don't want that.
		pc, filename, line, _ := runtime.Caller(1)

		log.Printf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), filename, line, err)
		b = true
	}
	return
}
